/**
 * Created by mtolstyh on 21.09.2016.
 */

import React from 'react'
import './TaskWidget.css'
import $ from 'jquery'
import App from './App.js'

var TaskWidget = React.createClass({

    getInitialState: function () {
        return {
            sort: {
                prop: 'date',
                direct: 'desc'
            },
            embedded: {task: []},
            links: {},
            page: {
                size: 5,
                number: 0,
                totalElements: 0,
                totalPages: 0
            }
        };
    },

    loadData(){
        var resource = App.api() + "task?" + 'size=' + this.state.page.size + '&page=' + this.state.page.number
            + '&sort=' + this.state.sort.prop + "," + this.state.sort.direct;
        $.get(resource)
            .done(function (resp) {

                this.setState({
                    embedded: resp._embedded,
                    links: resp._links,
                    page: resp.page
                })

            }.bind(this));
    },

    componentDidMount: function () {
        this.loadData();
    },

    previousPage: function () {
        if (this.state.page.number > 0) {

            var newState = this.state;

            newState.page.number = this.state.page.number - 1;

            this.setState(newState);

            this.loadData();
        }
    },

    nextPage: function () {
        if (this.state.page.number < this.state.page.totalPages - 1) {
            var newState = this.state;

            newState.page.number = this.state.page.number + 1;

            this.setState(newState);

            this.loadData();
        }

    },

    render: function () {

        return (
            <div className="TaskWidget-Content">

                <div className="TaskWidget-Content-Table-Header">
                    Tasker track
                </div>

                <div className="TaskWidget-Content-Table">

                    {this.state.embedded.task.map(function (task) {
                        return (
                            <div key={task.number} className="TaskWidget-Task animated infinity fadeInUp">
                                <div>{task.number} {task.title}</div>
                                <div className="TaskWidget-Task-Task">{task.task}</div>
                                <div>
                                    {new Date(task.date).getDay()}.{new Date(task.date).getMonth()}.{new Date(task.date).getFullYear()}
                                    {'  '}
                                    {new Date(task.date).getHours()}:{new Date(task.date).getMinutes()}
                                    {' -- '}
                                    {new Date(task.end).getDay()}.{new Date(task.end).getMonth()}.{new Date(task.end).getFullYear()}
                                    {'  '}
                                    {new Date(task.end).getHours()}:{new Date(task.end).getMinutes()}
                                </div>

                                { task.status ?
                                <div className="TaskWidget-Task-Task-State-True"></div> :
                                <div className="TaskWidget-Task-Task-State-False"></div>
                                }

                            </div>
                        )
                    })}

                </div>

                <div className="TaskWidget-Content-Table-Footer">

                    {(this.state.page.number + 1) * this.state.page.size} of {this.state.page.totalElements}
                    {'\t\t'}
                    <button onClick={this.previousPage}>{'<'}</button>
                    {' '}
                    {this.state.page.number + 1}/{this.state.page.totalPages}
                    {' '}
                    <button onClick={this.nextPage}>{'>'}</button>

                </div>
            </div>
        )
    }
});

export default TaskWidget;