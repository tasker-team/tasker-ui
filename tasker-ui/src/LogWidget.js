/**
 * Created by max_tolstykh on 19/09/16.
 */

import React, { Component } from 'react';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

class LogWidget extends Component {

    sock = undefined;

    data = undefined;

    client = undefined;

    static handleData(data) {
        console.log(data);
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.sock = new SockJS('http://localhost:8080/tasker-ws');

        this.sock.onmessage = this.sockOnMessage;

        var stompClient = Stomp.over(this.sock);

        stompClient.connect({}, function (frame) {

            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/dashboard/log', function (data) {
                console.log('log', data);
            });

            stompClient.subscribe('/topic/dashboard/alarm', function (data) {
                console.log('alarm', data);
            });
        });

        this.client = stompClient;
    }

    static send() {
        //this.client.send('/dashboard/log',{},);
    }

    sockOnMessage(data) {
        console.log(data);
    }

    componentWillUnmount() {
        this.sock.close();
    }

    render() {
        return (
            <div/>
        );
    }
}

export default LogWidget;