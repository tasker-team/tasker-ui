import React from 'react';
import LoginForm from './LoginForm';
import Dashboard from './Dashboard';

var App = React.createClass ({

    statics: {
        proto: 'http://',
        host: 'localhost',
        port: '8080/',
        path: '/task',

        api: function() {
            return App.proto + App.host + ":" + App.port;
        }
    },

    getInitialState: function() {
        return { loggedIn : false };
    },

    logout: function() {
        this.setState({loggedIn : false});
    },

    onPwdKeyPress: function(event) {

        if (event.type === 'keypress') {
            if(event.key === 'Enter') {

                //make logining to backend
                this.setState({loggedIn: true});
            }
        }
    },

    render: function() {
        return (this.state.loggedIn ? <Dashboard Logout={this.logout}/> : <LoginForm onPwdKeyPress={this.onPwdKeyPress}/>);
    }

});

export default App;
