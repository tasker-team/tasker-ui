/**
 * Created by mtolstyh on 21.09.2016.
 */

import React from 'react';
import CalendarWidget from './CalendarWidget.js';
import TaskWidget from './TaskWidget.js';
import JiraWidget from './JiraWidget.js';
import './Dashboard.css';

var Dashboard = React.createClass({

    getInitialState: function () {
        return {
            carret: '_'
        }
    },

    carretGo: function() {

        if(this.state.carret === '') {
            this.setState({
                carret: '_'
            });
        } else {
            this.setState({
                carret: ''
            });
        }

        setTimeout(this.carretGo, 500);
    },

    componentDidMount: function() {
        this.carretGo();
    },

    render: function () {
        return (
            <div className="Dashboard">

                <div className="Dashboard-Header">

                    <div className="Dashboard-Header-Left">
                        {'$>tsk -t' + this.state.carret}
                    </div>

                    <div className="Dashboard-Header-Right">
                        <button onClick={this.props.Logout}>Logout</button>
                    </div>
                </div>

                <div className="Dashboard-Content">
                    <TaskWidget />
                    <CalendarWidget />
                    <JiraWidget API_url='http://jira.taskdata.com/rest/' title='Taskdata track' key='TD' jira_user="mtolstyh"/>

                </div>
            </div>)
    }
});

export default Dashboard;

//<JiraWidget API_url='https://devcloud.swcoe.ge.com/jira03/rest/' title='GE track' key='GE' jira_user="502553832"/>