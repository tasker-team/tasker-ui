import React from 'react';
import logo from './logo.png';
import './App.css';

var LoginForm = React.createClass ({
    render: function() {
        return (
            <div className="App">

                <div className="App-header">

                    <img src={logo} className="App-logo" alt="logo"/>

                </div>

                <div className="animated infinity fadeInRightBig">
                    <div className="mainHeader">Tasker</div>
                </div>
                <div className="animated infinity fadeInLeftBig">
                    <div className="secondHeader">
                        Nothing for you
                    </div>
                </div>

                <div className="loginForm">
                    <input className="animated infinity fadeInLeftBig" placeholder="username"/>
                    <input className="animated infinity fadeInRightBig" placeholder="password" type="password"
                           onKeyPress={this.props.onPwdKeyPress}/>
                </div>
            </div> )
    }

});

export default LoginForm;