/**
 * Created by mtolstyh on 22.09.2016.
 */

import React from 'react'
import './JiraWidget.css'
import $ from 'jquery'
import App from './App.js'

var JiraWidget = React.createClass({

    getInitialState: function () {
        return {
            issues: [],
            maxResults: 10,
            startAt: 0,
            total: 0,
            session: {}
        }
    },

    loadData(){

        if (this.props.API_url) {
            var resource = this.props.API_url + "api/2/search";

            var body = {

                api: resource,

                headers: {
                    cookie: this.state.session.name + "=" + this.state.session.value,
                    "Content-Type": "application/json"
                },
                params: {
                    jql: "assignee=" + this.props.jira_user + "%20AND%20resolution=Unresolved",
                    maxResults: this.state.maxResults,
                    startAt: this.state.startAt
                }
            };

            $.post({
                url: App.api() + "dashboard/api/custom/get",
                data: JSON.stringify(body),
                headers: {
                    "Content-Type": "application/json"
                }
            }).done(function (resp) {

                var state = this.state;

                var data = JSON.parse(resp);

                state.issues = data.issues;

                state.maxResults = data.maxResults;

                state.startAt = data.startAt;

                this.setState(state);

            }.bind(this));
        }
    },

    login: function () {

        if (this.props.API_url) {
            var resource = this.props.API_url + "auth/1/session";

            $.get({
                url: App.api() + "dashboard/api?api=" + resource
            }).done(function (resp) {
                console.log(resp);
            }).fail(function (resp) {
                    console.log(resp);
                    $.post({
                        url: App.api() + "dashboard/api",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify({
                            api: resource,
                            jsonData: {"username": '****', "password": '****'}
                        })
                    }).done(function (resp) {

                        var data = JSON.parse(resp);

                        var state = this.state;

                        state.session = data.session;

                        this.setState(state);

                        this.loadData();
                    }.bind(this));

                }.bind(this)
            );

        }
    },

    componentDidMount: function () {
        this.login();
    },

    render: function () {
        return (
            <div className="JiraWidget-Content">
                <div className="JiraWidget-Content-Header">
                    {this.props.title}
                </div>
                <div className="JiraWidget-Content-Task">
                    {
                        this.state.issues.map(function (issue) {
                            return (
                                <div className="JiraWidget-Task animated infinity fadeInUp" key={issue.key} style={{border: '1px dashed', padding: '5px'}}>
                                    <div style={{padding: '5px', margin: '3px'}}>

                                        <img src={issue.fields.issuetype.iconUrl}/>
                                        {issue.key} <b>{issue.fields.project.name}</b>

                                    </div>
                                    <div style={{fontSize: '18px', padding: '5px', margin: '3px'}}>
                                        {issue.fields.summary}
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="JiraWidget-Content-Footer">

                </div>
            </div>
        );
    }
});

export default JiraWidget;