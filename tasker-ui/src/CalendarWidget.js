/**
 * Created by mtolstyh on 21.09.2016.
 */

import React from 'react';
import './CalendarWidget.css';

var CalendarWidget = React.createClass({

        getInitialState: function () {
            return {
                h: new Date().getHours(),
                m: new Date().getMinutes(),
                s: new Date().getSeconds()
            }
        },

        startTime: function () {
            var today = new Date();
            var hh = today.getHours();
            var mm = today.getMinutes();
            var ss = today.getSeconds();

            this.setState({
                h: hh,
                m: mm,
                s: ss
            });

            setTimeout(this.startTime, 500);
        },

        componentDidMount: function() {
            this.startTime();
        },

        render: function () {
            return (
                <div className="CalendarWidget-Content">
                    <div className="CalendarWidget-Time">
                        {this.state.h} : {this.state.m} : {this.state.s}
                    </div>
                    <div className="CalendarWidget-Calendar">
                        <iframe
                            src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;src=maxim.tolstyh%40taskdata.ru&amp;color=%2329527A&amp;ctz=Europe%2FMoscow"
                            width="200" height="150" frameBorder="0" scrolling="no"></iframe>
                    </div>
                </div>
            );
        }
    })
    ;

export default CalendarWidget;


//<div>
//    <iframe
//        src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;src=maxim.v.tolstyh%40gmail.com&amp;color=%2329527A&amp&ctz=Europe/Moscow"
//        width="500" height="400" frameBorder="0" scrolling="no"></iframe>
//</div>

setInterval(function () {

}, 500);